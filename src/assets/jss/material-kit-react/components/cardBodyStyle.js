const cardBodyStyle = {
  cardBody: {
    padding: "0.9375rem 1.875rem",
    flex: "1 1 auto",
    background: '#153064',
    minHeight: '400px',

    "@media (max-width: 700px)": {
      width: "100%",
      minHeight: '200px',
      padding: "0",
    }
  }
};

export default cardBodyStyle;
