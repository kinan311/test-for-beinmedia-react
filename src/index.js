import React from "react";
import ReactDOM from "react-dom";
import { createBrowserHistory } from "history";
import { Router, Route, Switch } from "react-router-dom";
import { ThemeProvider } from '@material-ui/styles';
import "assets/scss/material-kit-react.scss?v=1.9.0";

// pages for this product
import Components from "views/Components/Components.js";
import LoginPage from "views/LoginPage/LoginPage.js";
import theme from './assets/jss/material-kit-react/CustomTheme';


var hist = createBrowserHistory();


ReactDOM.render(

<ThemeProvider theme={theme}>
 <Router history={hist}>
    <Switch>
      <Route path="/login-page" component={LoginPage} /> 
      <Route path="/" component={Components} />
    
    </Switch>
  </Router>
  </ThemeProvider>
 
  ,
  document.getElementById("root")
);
