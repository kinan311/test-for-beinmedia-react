import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import styles from "assets/jss/material-kit-react/components/typographyStyle.js";
import {courserModel} from "../../Models/CourseModel"
import loadable from '@loadable/component'
const CustomMobilePlayer = loadable(() => import('components/CustomVideoPlayer/CustomMobilePlayer'))
const Button = loadable(() => import('components/CustomButtons/Button.js'));
const CustomCardList = loadable(() => import('components/CustomCardList/CusromCardList'));
const GridContainer = loadable(() => import('components/Grid/GridContainer.js'));
const CustomPlayer = loadable(() => import('components/CustomVideoPlayer/CustomPlayer'));
const GridItem = loadable(() => import('components/Grid/GridItem.js'));
const Quote = loadable(() => import('components/Typography/Quote.js'));


const useStyles = makeStyles(styles);

export default function ViewCoursePage(props) {

    const classes = useStyles();
    const [course, setCourse] = React.useState(courserModel)

    const MobilePlayer = () =>{
        if(props.isMobile === true){
            return (
                <GridItem xs={12} sm={12} md={7} >
                    <CustomMobilePlayer url='' isMobile={props.isMobile} />
                </GridItem>
            )
        } 
    } 

    const WebPlayer = () => {
        if(props.isMobile === false ) {
            return (
                <GridItem xs={12} sm={12} md={7} >
                    <CustomPlayer  url=''  isWeb={props.isWeb} />
                </GridItem>
            )
        } 
    }

    return (
        <GridContainer justify="space-between"  >

           
            <GridItem xs={12} sm={12} md={5} className={props.isMobile ? classes.mobileCardContainer :classes.cardContainer}>

                <div className={props.isMobile ? classes.quoteMobileContainer : classes.quoteContainer}>
                    <h3 className={props.isMobile ? classes.mobileQuoteTitle : classes.quoteTitle}>وصف الكورس</h3>
                    <Quote isMobile={props.isMobile}
                        text="هند الناهض مستشارة إعلام رقمي, مستشارة الإعلام الإلكتروني لمكتب وكيل وزارة الإعلام الكويتية وهي المؤسسة لشركة “سوشالوبي” للخدمات المختصة في مجال الإعلام الإجتماعي , بالإضافة الى إنها مستشارة في مجال التواصل الاجتماعي.هند الناهض مستشارة إعلام رقمي, مستشارة الإعلام الإلكتروني لمكتب وكيل وزارة الإعلام الكويتية وهي المؤسسة لشركة “سوشالوبي” للخدمات المختصة في مجال الإعلام الإجتماعي , بالإضافة الى إنها مستشارة في مجال التواصل الاجتماعي."
                    />
                </div>
                {MobilePlayer()}
                {props.isMobile ?   <Button className={classes.reserveBtCourseDetailnMobile}>حجز عيادة(20 دينار كويتي)</Button> : ''}
                {props.isMobile ? <div className={classes.space}></div>  : ''}
                {course.map((course) => (
                    <CustomCardList courseList={false} imgSrc={course.imgSrc} courseText={course.courseText}  key={course.id} courseSalary={course.courseSalary} />
                ))}
                


            </GridItem>
            

             {WebPlayer()}
            
        </GridContainer>

        
    )
}