import React from "react";
import { makeStyles } from "@material-ui/core/styles"; 
import styles from "assets/jss/material-kit-react/components/typographyStyle.js";
import {experienceModel} from "../../Models/CourseModel"
import loadable from '@loadable/component'
const CustomMobilePlayer = loadable(() => import('components/CustomVideoPlayer/CustomMobilePlayer'))
const Button = loadable(() => import('components/CustomButtons/Button.js'));
const Quote = loadable(() => import('components/Typography/Quote.js'));
const GridContainer = loadable(() => import('components/Grid/GridContainer.js'));
const CustomPlayer = loadable(() => import('components/CustomVideoPlayer/CustomPlayer'));
const GridItem = loadable(() => import('components/Grid/GridItem.js'));

const useStyles = makeStyles(styles);

export default function AboutPage(props) {
    const classes = useStyles();
    const [mobileView, setMobileView] = React.useState(false)
    const [experience , setExperience] = React.useState(experienceModel)

    const MobilePlayer = () => {
        if (props.isMobile === true) {
            return (
                <GridItem xs={12} sm={12} md={7} >
                    <CustomMobilePlayer url='' isMobile={props.isMobile} isWeb={props.isWeb} />
                </GridItem>

            )
        }
    }

    const WebPlayer = () => {
        if (props.isMobile === false) {
            return (

                <GridItem xs={12} sm={12} md={7} >
                    <CustomPlayer url='' isWeb={props.isWeb} isMobile={props.isMobile} />
                </GridItem>

            )
        }
    }

    return(
        
        <GridContainer justify="center" >

            {MobilePlayer()}

            <GridItem xs={12} sm={12} md={4} className={props.isMobile ? classes.mobileContainer :classes.container}>

             <div className={classes.quoteContainer}>
                 {props.isMobile ?   <Button className={classes.reserveBtnMobile}>حجز عيادة(20 دينار كويتي)</Button> : ''}
               
             <Quote
                text="هند الناهض مستشارة إعلام رقمي, مستشارة الإعلام الإلكتروني لمكتب وكيل وزارة الإعلام الكويتية وهي المؤسسة لشركة “سوشالوبي” للخدمات المختصة في مجال الإعلام الإجتماعي , بالإضافة الى إنها مستشارة في مجال التواصل الاجتماعي.هند الناهض مستشارة إعلام رقمي, مستشارة الإعلام الإلكتروني لمكتب وكيل وزارة الإعلام الكويتية وهي المؤسسة لشركة “سوشالوبي” للخدمات المختصة في مجال الإعلام الإجتماعي , بالإضافة الى إنها مستشارة في مجال التواصل الاجتماعي."
            />
            </div>

                
            <div dir='rtl'>
                 <h4 className={ props.isMobile ? classes.mobileTitleTag : classes.titleTag} dir="rtl">الخبرات</h4>
                 {props.isMobile ?  <div className={classes.mobileViewButtons}>
                  {experience.map((exp) => (

                      <Button color="primary" key={exp.id} size="sm" className={props.isMobile ? classes.mobileBtn : ''} >
                          {exp.btnText}
                      </Button>
                  ))}
                  </div>
                   : 
                   <div>
                  {experience.map((exp) => (

                      <Button color="primary" key={exp.id} size="sm" >
                          {exp.btnText}
                      </Button>
                  ))}
                  </div>
                  }
            </div>

            </GridItem>
            
           
                {WebPlayer()}
        

            </GridContainer>
    )
}