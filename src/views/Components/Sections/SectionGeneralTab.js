import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import styles from "assets/jss/material-kit-react/views/componentsSections/tabsStyle.js";
import loadable from '@loadable/component'
const AboutPage = loadable(() => import('../AboutPage/AboutPage'))
const StorePage = loadable(() => import('../StorePage/StorePage'));
const BroadcastPage = loadable(() => import('../BroadcastPage/BroadcastPage'));
const GridContainer = loadable(() => import('components/Grid/GridContainer.js'));
const GridItem = loadable(() => import('components/Grid/GridItem.js'));
const CoursesPage = loadable(() => import('../CoursesPage/CoursesPage'));
const CustomTabs = loadable(() => import('components/CustomTabs/CustomTabs.js'));


const useStyles = makeStyles(styles);

export default function SectionGeneralTab(props) {

    console.log('new props' , props)
    const classes = useStyles();
    const [value, setValue] = React.useState(0);
    const [showViewIcon,setView] = React.useState(false);

    const [activatedTab, setTab] = React.useState(0);
    const isMobile = props.isMobile;
    const isWeb = props.isWeb
    const handleChange = (event, value) => {
        setValue(value);
    };

    

    const handleViewIcon = (viewStatus) => {
        
        setView(viewStatus);
    }

   const  handleHideIcon = (hideValue) => {
        console.log('hide value ', hideValue)
        setView(false)
    }
    
    return (
        <GridContainer dir="rtl" >
            <GridItem xs={12} sm={12} md={12} classNames={classes.cardContainer}>

                            <CustomTabs
                                plainTabs
                                headerColor="primary"
                                isMobile={isMobile}
                                handleChange={handleChange}
                                viewIcon={showViewIcon}
                                hideIcon={handleHideIcon}
                                active = {0}
                                
                                isOpen={props.isOpen}
                                tabs={[
                                    {
                                        tabName: "نبذة عن هند",
                                         disabled: false,
                                        tabContent: (
                                            <AboutPage isMobile={isMobile} isWeb={isWeb} />
                                        )
                                    },
                                    {
                                        tabName: "متجر",
                                         disabled: false,
                                        tabContent: (
                                            <StorePage isMobile={isMobile} isWeb={isWeb} />
                                        )
                                    },
                                    {
                                        tabName: "برودكاست",
                                         disabled: false,
                                        tabContent: (
                                            <BroadcastPage isMobile={isMobile} isWeb={isWeb} />
                                        )
                                    },
                                    {
                                        tabName: "كورسات",
                                        disabled: false,
                                        tabIcon: { name : 'fas fa-arrow-right'},
                                        tabContent: (
                                            <CoursesPage hideViewCourse={showViewIcon} showArrowIcon={handleViewIcon} changeActiveTab={activatedTab => {
                                                setTab(activatedTab)}} 
                                                isMobile={isMobile} isWeb={isWeb} />
                                        ),
                                        styleIcon: {
                                            padding: '0 5px'
                                            }
                                    },
                                    {
                                        tabName: "حجز عيادة (20 دينار كويتي) ",
                                        disabled: true ,
                                        tabContent: (
                                            <p>
                                            </p>
                                        )
                                    },
                                   
                                ]}
                            />
                           
                        </GridItem>
        </GridContainer>
          
    )
}