import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import styles from "assets/jss/material-kit-react/views/componentsSections/navbarsStyle.js";
import logoImage from "assets/img/logo/logo.png";
import profileImg from "assets/img/profile.webp";

import loadable from '@loadable/component'
const Header = loadable(() => import('components/Header/Header.js'))
const Primary = loadable(() => import('components/Typography/Primary.js'));
const SecondPrimary = loadable(() => import('components/Typography/SecondPrimary'));
const GridContainer = loadable(() => import('components/Grid/GridContainer.js'));
const GridItem = loadable(() => import('components/Grid/GridItem.js'));
const Badge = loadable(() => import('components/Badge/Badge.js'));



export default function SectionFileTab(props) {
    
    const useStyles = makeStyles(styles);
    const classes = useStyles();
    return (
        <Header
            color="primary"
            className={classes.fileTab}
            isMobile={props.isMobile}
            container
            rightLinks={
            <img
                src={logoImage}
                className={classes.logoImg}
                alt="logo"
            />}
            leftLinks={
                <>
            
                <List className={classes.list}>
                    <ListItem className={classes.listItem}>
                        <i class="fas fa-home fa-s" className={classes.homeIcon} style={{ color: '#435f95' }}></i>
                    </ListItem>

                    <ListItem className={classes.listItem}>
                        <img
                            src={profileImg}
                            className={classes.img}
                            alt="profile"
                        />
                    </ListItem>

                    <GridContainer className={classes.listItem}>
                     
                        <GridItem className={classes.personalInfoContainer}>
                            <Primary>العيادة الرقمية</Primary>
                            <SecondPrimary>ل د.هند الناهض</SecondPrimary>
                            <ListItem className={classes.gutters}  ><SecondPrimary >تقييم</SecondPrimary><i class="fas fa-star fa-xs" style={{ color: '#435f95', margin: '0px  2px '}}></i><i class="fas fa-star fa-xs" style={{ color: '#E75C50', margin: '0px  2px ' }}></i><i class="fas fa-star fa-xs" style={{ color: '#E75C50', margin: '0px  2px ' }}></i><i class="fas fa-star fa-xs" style={{ color: '#E75C50', margin: '0px  2px ' }}></i></ListItem>

                        </GridItem>
                      
                    </GridContainer>

                    
                   
                   
                    <ListItem className={classes.listItem}>
                        <Badge color="gradiant" className={classes.Badge}>مشغول باستشارة</Badge>
                    </ListItem>
                </List>
                </>
            }
        />

    )
}