
import team1 from "assets/img/faces/faceCard.webp";
import team2 from "assets/img/svgBG/videoBG22.webp";
import team3 from "assets/img/svgBG/videoBG33.webp";
import team4 from "assets/img/svgBG/videoBG44.webp";

export const courserModel = [
        { courseText: 'تعرف على أسرار صناعة المحتوى التسويقي الفعال', id: '1', imgSrc: team1, courseSalary: '299' , coins: 'دينار كويتي'},
        { courseText: 'تعرف على أسرار صناعة المحتوى التسويقي الفعال', id: '2', imgSrc: team2, courseSalary: '340' , coins: 'دينار كويتي'},
        { courseText: 'دورة صناعة المحتوى مع هند الناهض', id: '3', imgSrc: team3, courseSalary: '500', coins: 'دينار كويتي' },
        { courseText: ' دورة صناعة المحتوى مع هند الناهض', id: '4', imgSrc: team4, courseSalary: '190', coins: 'دينار كويتي' },
        { courseText: 'فوكس حلقة ٥', id: '5', imgSrc: team1, courseSalary: '299', coins: 'دينار كويتي' },
        { courseText: ' تعرف على أسرار صناعة المحتوى التسويقي الفعال', id: '6', imgSrc: team2, courseSalary: '299', coins: 'دينار كويتي' },
        { courseText: ' دورة صناعة المحتوى مع هند الناهض', id: '7', imgSrc: team4, courseSalary: '190', coins: 'دينار كويتي' },
        { courseText: 'فوكس حلقة ٥', id: '8', imgSrc: team3, courseSalary: '299' ,coins: 'دينار كويتي'},
        { courseText: ' تعرف على أسرار صناعة المحتوى التسويقي الفعال', id: '9', imgSrc: team2, courseSalary: '299', coins: 'دينار كويتي' },
        { courseText: ' دورة صناعة المحتوى مع هند الناهض', id: '10', imgSrc: team1, courseSalary: '190', coins: 'دينار كويتي' },
        { courseText: 'فوكس حلقة ٥', id: '11', imgSrc: team1, courseSalary: '299', coins: 'دينار كويتي' },
        { courseText: ' تعرف على أسرار صناعة المحتوى التسويقي الفعال', id: '12', imgSrc: team2, courseSalary: '299', coins: 'دينار كويتي' },
    ];


    export const storeCourseModel = [
        { courseText: 'تعرف على أسرار صناعة المحتوى التسويقي الفعال', id: '1', imgSrc: team1, courseSalary: '299 دينار كويتي', url: team1},
        { courseText: 'تعرف على أسرار صناعة المحتوى التسويقي الفعال', id: '2', imgSrc: team2, courseSalary: '340 دينار كويتي', url: ''},
        { courseText: 'دورة صناعة المحتوى مع هند الناهض', id: '3', imgSrc: team3, courseSalary: '500 دينار كويتي', url: ''},
        { courseText: ' دورة صناعة المحتوى مع هند الناهض', id: '4',fileType: 'PDF',  courseSalary: '190 دينار كويتي', url: ''},
        { courseText: 'تعرف على أسرار صناعة المحتوى', id: '5', fileType: 'Word',  courseSalary: '299 دينار كويتي', url: ''},
        { courseText: ' تعرف على أسرار صناعة المحتوى ', id: '6', fileType: 'Audio', courseSalary: '299 دينار كويتي', url: ''},
    ];


    export const experienceModel = [
        {btnText: 'تطوير الأعمال', id: '1' , key: '1'},
        { btnText: 'المشاريع الصغيرة', id: '2', key: '2' },
        { btnText: 'مشاركة العملاء', id: '3', key: '3' }, 
        { btnText: 'البحث أمام الجمهور', id: '4', key: '4' },
        { btnText: 'الدعاية والإعلان', id: '5',key: '5' },
        { btnText: 'ريادة الأعمال', id: '6', key: '6' },
        { btnText: 'التسويق الرقمي', id: '7', key: '7' }, 
        { btnText: 'الإعلام', id: '8', key: '8' },
    ]


    export const broadcastModel = [
        { courseText: 'تعرف على أسرار صناعة المحتوى التسويقي الفعال', id: '1', imgSrc: team1, courseSalary: '299 دينار كويتي' },
        { courseText: 'تعرف على أسرار صناعة المحتوى التسويقي الفعال', id: '2', imgSrc: team2, courseSalary: '340 دينار كويتي' },
        { courseText: 'دورة صناعة المحتوى مع هند الناهض', id: '3', imgSrc: team3, courseSalary: '500 دينار كويتي' },
        { courseText: ' دورة صناعة المحتوى مع هند الناهض', id: '4', imgSrc: team4, courseSalary: '190 دينار كويتي' },
        { courseText: 'فوكس حلقة ٥', id: '5', imgSrc: team1, courseSalary: '299 دينار كويتي' },
        { courseText: ' تعرف على أسرار صناعة المحتوى التسويقي الفعال', id: '6', imgSrc: team2, courseSalary: '299 دينار كويتي' },
        { courseText: ' دورة صناعة المحتوى مع هند الناهض', id: '7', imgSrc: team4, courseSalary: '190 دينار كويتي' },
        { courseText: 'فوكس حلقة ٥', id: '8', imgSrc: team3, courseSalary: '299 دينار كويتي' },
        { courseText: ' تعرف على أسرار صناعة المحتوى التسويقي الفعال', id: '9', imgSrc: team2, courseSalary: '299 دينار كويتي' },
        { courseText: ' دورة صناعة المحتوى مع هند الناهض', id: '10', imgSrc: team1, courseSalary: '190 دينار كويتي' },
        { courseText: 'فوكس حلقة ٥', id: '11', imgSrc: team4, courseSalary: '299 دينار كويتي' },
        { courseText: ' تعرف على أسرار صناعة المحتوى التسويقي الفعال', id: '12', imgSrc: team3, courseSalary: '299 دينار كويتي' },
        { courseText: ' دورة صناعة المحتوى مع هند الناهض', id: '13', imgSrc: team2, courseSalary: '190 دينار كويتي' },
        { courseText: 'فوكس حلقة ٥', id: '14', imgSrc: team1, courseSalary: '299 دينار كويتي' },
        { courseText: 'فوكس حلقة ٥', id: '15', imgSrc: team1, courseSalary: '299 دينار كويتي' },

    ];