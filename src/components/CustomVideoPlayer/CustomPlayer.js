import React from "react";
import classNames from "classnames";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import styles from "assets/jss/material-kit-react/components/cardStyle.js";
import overlay from "../../assets/img/overlay.svg"
import audioIcon from "../../assets/img/video.svg"
import fullScreen from "../../assets/img/fullScreenBtn.svg"
import volume from "../../assets/img/volume.svg"

const useStyles = makeStyles(styles);


export default function CustomPlayer(props) {
    const classes = useStyles();
    const { className, children, plain, carousel, ...rest } = props;
    const cardClasses = classNames({
        [classes.card]: true,
        [classes.cardPlain]: plain,
        [classes.cardCarousel]: carousel,
        [className]: className !== undefined
    });



    return (
        <React.Fragment>
            
        <div
            className={classes.videoPlayer}
          >
            <img src={overlay} alt="..." className={classes.bgOverlay} />
            <img src={audioIcon} alt="..." className={classes.audioOverlay} />
            <span className={classes.lineOverlay} ></span>
            <img src={volume} alt="..." className={classes.volumeOverlay} />
            <img src={fullScreen} alt="..." className={classes.fullscreenOverlay} />

             <span className={classes.overLabel}>
                {props.courseText ? props.courseText : 'المقدمة التعريفية ل هند الناهض'}
            </span>
          </div>        
       
        </React.Fragment>
        


       
    );

  
}


CustomPlayer.propTypes = {
    className: PropTypes.string,
    plain: PropTypes.bool,
    carousel: PropTypes.bool,
    children: PropTypes.node
};