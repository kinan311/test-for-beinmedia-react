import React from "react";
// nodejs library that concatenates classes
import classNames from "classnames";
// nodejs library to set properties for components
import PropTypes from "prop-types";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import Button from "@material-ui/core/Button";
import Hidden from "@material-ui/core/Hidden";
import Drawer from "@material-ui/core/Drawer";
import SwipeableDrawer from '@material-ui/core/SwipeableDrawer';
// @material-ui/icons
import Menu from "@material-ui/icons/Menu";
// core components
import styles from "assets/jss/material-kit-react/components/headerStyle.js";

const useStyles = makeStyles(styles);

export default function Header(props) {
  
  console.log('proops new sssssss', props);
  const classes = useStyles();
  const [mobileOpen, setMobileOpen] = React.useState(false);
  const [mobileMenuOpen, setMobileMenuOpen] = React.useState(false);
  React.useEffect(() => {
    if (props.changeColorOnScroll) {
      window.addEventListener("scroll", headerColorChange);
    }
    return function cleanup() {
      if (props.changeColorOnScroll) {
        window.removeEventListener("scroll", headerColorChange);
      }
    };
  });
  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  };

  const handleMenuDrawerToggle = () => {
    setMobileMenuOpen(!mobileMenuOpen)
  }

  const headerColorChange = () => {
    const { color, changeColorOnScroll } = props;
    const windowsScrollTop = window.pageYOffset;
    if (windowsScrollTop > changeColorOnScroll.height) {
      document.body
        .getElementsByTagName("header")[0]
        .classList.remove(classes[color]);
      document.body
        .getElementsByTagName("header")[0]
        .classList.add(classes[changeColorOnScroll.color]);
    } else {
      document.body
        .getElementsByTagName("header")[0]
        .classList.add(classes[color]);
      document.body
        .getElementsByTagName("header")[0]
        .classList.remove(classes[changeColorOnScroll.color]);
    }
  };
  const { color, rightLinks, leftLinks, brand, fixed, absolute } = props;
  const appBarClasses = classNames({
    [classes.appBar]: true,
    [classes[color]]: color,
    [classes.absolute]: absolute,
    [classes.fixed]: fixed
  });
  const brandComponent = <Button className={classes.title}>{brand}</Button>;
  return (
    <AppBar className={appBarClasses}>
      <Toolbar className={classes.container}>
        {leftLinks !== undefined ? brandComponent : null}
        <div className={classes.flex}>
          
          {props.isMobile ? (
            <Hidden className={classes.logoContainer} implementation="css">
              <Hidden mdUp>
                <IconButton
                  color="inherit"
                  aria-label="open menu drawer"
                  onClick={handleDrawerToggle}
                >
                  {leftLinks}
                </IconButton>
              </Hidden> 
            </Hidden>
          ) : (
            leftLinks
          )}
        </div>
        
        <Hidden smDown implementation="css" className={classes.leftLogo}>
          {rightLinks}
        </Hidden>

        <Hidden mdUp>
          <IconButton
            color="inherit"
            aria-label="open menu drawer"
            onClick={handleMenuDrawerToggle}
          >
            <Menu />
          </IconButton>
        </Hidden>
      </Toolbar>
      {props.isMobile ?
       <Hidden mdUp implementation="js">
        <Drawer
          variant="temporary"
          anchor={"right"}
          open={mobileMenuOpen}
          classes={{
            paper: classes.drawerMenuPaper,
            root: classes.drawerRoot
          }}
          onClose={handleMenuDrawerToggle}
        >
          <div className={classes.appResponsive}>
            <ul className={classes.menuDrawerContainer}>
              <p className={classes.menuItem}>الرئيسية</p>
              <p className={classes.menuItem}>نبذه عنا</p>
              <p className={classes.menuItem}>الخبراء</p>
              <p className={classes.menuItem}>سؤال وجواب</p>
              <p className={classes.menuItem}>إختبارات كورونا</p>
              <p className={classes.menuItem}>إتصل بنا </p>
              <p className={classes.menuItem}>إنضم كخبير</p>
              <p className={classes.menuItem} style={{fontWeight: 'bold'}} onClick={handleMenuDrawerToggle}>إغلاق</p>
            </ul>
            {/* {leftLinks}
            {rightLinks}  */}
          </div>
        </Drawer>
      </Hidden> :

       ''
       }
      


       <Hidden mdUp implementation="js">
        <SwipeableDrawer
          variant="temporary"
          anchor={"right"}
          open={mobileOpen}
          
          classes={{
            paper: classes.drawerPaper,
            root: classes.drawerRoot
          }}
          onClose={handleDrawerToggle}
        >
          <div className={classes.appResponsive}>
            <div onClick={handleDrawerToggle}>
            {leftLinks}
             </div>
            {rightLinks}
          
          </div>
        </SwipeableDrawer>
      </Hidden>
    </AppBar>
  );
}

Header.defaultProp = {
  color: "white"
};

Header.propTypes = {
  color: PropTypes.oneOf([
    "primary",
    "info",
    "success",
    "warning",
    "danger",
    "transparent",
    "white",
    "blue",
    "dark"
  ]),
  rightLinks: PropTypes.node,
  leftLinks: PropTypes.node,
  brand: PropTypes.string,
  fixed: PropTypes.bool,
  absolute: PropTypes.bool,

  changeColorOnScroll: PropTypes.shape({
    height: PropTypes.number.isRequired,
    color: PropTypes.oneOf([
      "primary",
      "info",
      "success",
      "warning",
      "danger",
      "transparent",
      "white",
      "blue",
      "dark"
    ]).isRequired
  })
};
